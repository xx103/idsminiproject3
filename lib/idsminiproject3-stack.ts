import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import * as iam from 'aws-cdk-lib/aws-iam';
// create an s3 bucket

export class S3Stack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    // Make an S3 bucket with versioning enabled and server-side encryption
    const bucket = new cdk.aws_s3.Bucket(this, 'idsminiproject3', {
      versioned: true,
      encryption: cdk.aws_s3.BucketEncryption.S3_MANAGED,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
    });
    
    
    bucket.grantRead(new iam.AccountRootPrincipal());

  }
}
